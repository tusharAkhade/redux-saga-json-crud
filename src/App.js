import "./App.css";
import {
  BrowserRouter,
  Switch,
  Route,
} from "react-router-dom/cjs/react-router-dom.min";
import Home from "./components/Home";
import AddEditUser from "./components/AddEditUser";
import UserInfo from "./components/UserInfo";
import Header from "./components/Header";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <ToastContainer />
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/add-user" component={AddEditUser} />
          <Route path="/edit-user/:id" component={AddEditUser} />
          <Route path="/user-info/:id" component={UserInfo} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
