import * as types from "../actions/actionTypes";
import {
  all,
  take,
  takeEvery,
  takeLatest,
  put,
  delay,
  fork,
  call,
} from "@redux-saga/core/effects";

import {
  loadUsersError,
  loadUsersSuccess,
  createUserSuccess,
  createUserError,
  deleteUserSuccess,
  deleteUserError,
  updateUserSuccess,
  updateUserError,
} from "../actions/usersAction";
import {
  loadUsersApi,
  createUserApi,
  deleteUserApi,
  updateUserApi,
} from "../api";

function* onLoadUsersStartAsync() {
  try {
    const response = yield call(loadUsersApi);
    if (response.status === 200) {
      yield delay(500);
      yield put(loadUsersSuccess(response.data));
    }
  } catch (e) {
    yield put(loadUsersError(e.message));
  }
}

// 'payload' is a 'userData' getting from user
function* onCreateUserStartAsync({ payload }) {
  try {
    const response = yield call(createUserApi, payload);
    if (response.status === 201) {
      yield put(createUserSuccess());
    }
  } catch (e) {
    yield put(createUserError(e.message));
  }
}

function* onDeleteUserStartSync(userId) {
  try {
    const response = yield call(deleteUserApi, userId);
    if (response.status === 200) {
      yield delay(500);
      yield put(deleteUserSuccess(userId));
    }
  } catch (e) {
    yield put(deleteUserError(e.message));
  }
}

function* onUpdateUserStartAsync({ payload }) {
  const { id, formValue } = payload;
  try {
    // console.log("Update user payload:", payload);
    const response = yield call(updateUserApi, id, formValue); // passing 'id' and 'formValue' as an argument to the 'updateUserApi()'
    if (response.status === 200) {
      yield put(updateUserSuccess());
    }
  } catch (e) {
    yield put(updateUserError(e.message));
  }
}

// Redux Saga for delete
function* onDeleteUser() {
  while (true) {
    // alias the 'payload' as 'userId'
    const { payload: userId } = yield take(types.DELETE_USER_START);
    yield call(onDeleteUserStartSync, userId); // passing 'userId' as an argument to the 'onDeleteUserStartSync()'
  }
}

// Redux Saga for read
function* onLoadUsers() {
  yield takeEvery(types.LOAD_USERS_START, onLoadUsersStartAsync);
}

// Redux Saga for create
function* onCreateUser() {
  yield takeLatest(types.CREATE_USER_START, onCreateUserStartAsync);
}

// Redux Saga for update
function* onUpdateUser() {
  yield takeLatest(types.UPDATE_USER_START, onUpdateUserStartAsync);
}

const userSagas = [
  fork(onLoadUsers),
  fork(onCreateUser),
  fork(onDeleteUser),
  fork(onUpdateUser),
];

export default function* rootSaga() {
  yield all([...userSagas]);
}
