import axios from "axios";

// API to read data
export const loadUsersApi = async () => {
  return await axios.get("http://localhost:5000/users");
};

// API to add data
export const createUserApi = async (userData) => {
  return await axios.post("http://localhost:5000/users", userData);
};

// API to delete data
export const deleteUserApi = async (userId) => {
  return await axios.delete(`http://localhost:5000/users/${userId}`);
};

// API to update data
export const updateUserApi = async (userId, userData) => {
  return await axios.put(`http://localhost:5000/users/${userId}`, userData);
};
