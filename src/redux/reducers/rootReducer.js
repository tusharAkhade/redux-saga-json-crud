import { combineReducers } from "redux";
import usersReducer from "./usersReducer";

const rootReducer = combineReducers({
  data: usersReducer,
});

export default rootReducer;
